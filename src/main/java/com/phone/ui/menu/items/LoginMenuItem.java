package com.phone.ui.menu.items;

import com.phone.repository.OwnerRepository;
import com.phone.service.RegisterLoginInputService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class LoginMenuItem implements RunnableMenuItem {

    private final RegisterLoginInputService registerLoginInputService;
    private final OwnerRepository ownerRepository;

    private boolean isBack = false;

    @Override
    public String getName() {
        return "Login";
    }

    @Override
    public void run() {


    }

    @Override
    public boolean isBack() {
        return isBack;
    }
}
