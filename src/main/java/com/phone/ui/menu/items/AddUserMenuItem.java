package com.phone.ui.menu.items;

import com.phone.repository.ContactRepository;
import com.phone.repository.model.ContactEntity;
import com.phone.repository.model.RecordEntity;
import com.phone.repository.model.TypeContact;
import com.phone.service.AddUserInputService;
import com.phone.service.RegisterLoginInputService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class AddUserMenuItem implements RunnableMenuItem {

    private final ContactRepository contactRepository;

    private final AddUserInputService addUserInputService;
    private final RegisterLoginInputService loginInputService;

    @Override
    public String getName() {
        return "Add Contract";
    }

    @Override
    public void run() {
        String name = addUserInputService.getName();
        String surname = addUserInputService.getSurname();
        String patronymic = addUserInputService.getPatronymic();

        ContactEntity user = new ContactEntity()
                .setName(name)
                .setSurname(surname)
                .setPatronymic(patronymic)
                .setOwner(loginInputService.getOwner());
        do {
            TypeContact typeContact = addUserInputService.getTypeContact();

            String valueContact = addUserInputService.getValueContact(typeContact);
            user.addRecord(new RecordEntity().setType(typeContact).setValue(valueContact));
        } while (addUserInputService.getMoreContact());

        contactRepository.save(user);

        System.out.println("Added contact!");
    }
}
