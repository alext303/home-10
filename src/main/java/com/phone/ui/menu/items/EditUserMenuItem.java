package com.phone.ui.menu.items;

import com.phone.repository.model.ContactEntity;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class EditUserMenuItem implements RunnableMenuItem, AddMenu {

    private final ContactEntity contact;

    @Override
    public String getName() {
        return "Edit Contact";
    }

    @Override
    public void run() {

    }

    @Override
    public List<MenuItem> getMenu() {
        return List.of(
                new BackMenuItem()
        );
    }
}
