package com.phone.ui.menu.items;

public class BackMenuItem implements MenuItem {

    @Override
    public String getName() {
        return "Back";
    }

    @Override
    public boolean isBack() {
        return true;
    }
}
