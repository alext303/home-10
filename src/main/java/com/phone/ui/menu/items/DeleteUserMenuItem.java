package com.phone.ui.menu.items;

import com.phone.repository.ContactRepository;
import com.phone.repository.RecordRepository;
import com.phone.repository.model.ContactEntity;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class DeleteUserMenuItem implements RunnableMenuItem {

    private final ContactEntity user;
    private final ContactRepository contactRepository;

    @Override
    public String getName() {
        return String.format("Delete contact (%S)", user.getSurname());
    }

    @Override
    public void run() {
        contactRepository.delete(user);
    }

    @Override
    public boolean isBack() {
        return true;
    }
}
