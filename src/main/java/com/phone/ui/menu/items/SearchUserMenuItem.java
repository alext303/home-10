package com.phone.ui.menu.items;

import com.phone.repository.ContactRepository;
import com.phone.service.RegisterLoginInputService;
import com.phone.service.SearchInputService;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class SearchUserMenuItem implements MenuItem, AddMenu {

    private final ContactRepository contactRepository;

    private final SearchInputService searchInputService;
    private final RegisterLoginInputService registerLoginInputService;

    @Override
    public String getName() {
        return "Search Users";
    }

    @Override
    public List<MenuItem> getMenu() {
        return List.of(
                new SearchByTypeSearchContainsMenuItem(TypeSearch.NAME, contactRepository, registerLoginInputService, searchInputService),
                new SearchByTypeSearchContainsMenuItem(TypeSearch.RECORD, contactRepository, registerLoginInputService, searchInputService),
                new BackMenuItem()
        );
    }
}
