package com.phone.ui.menu.items;

import com.phone.repository.ContactRepository;
import com.phone.repository.model.ContactEntity;
import com.phone.repository.model.RecordEntity;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class UserMenuItem implements RunnableMenuItem, AddMenu {

    private final ContactEntity contact;
    private final ContactRepository contactRepository;

    private boolean isBack;

    @Override
    public String getName() {
        return String.format("%s %s. %s.", contact.getSurname(), contact.getName().charAt(0), contact.getPatronymic().charAt(0));
    }

    @Override
    public void run() {
        System.out.println("Info: ");
        System.out.printf("Name: %s\n", contact.getName());
        System.out.printf("Surname: %s\n", contact.getSurname());
        System.out.printf("Patronymic: %s\n", contact.getPatronymic());

        for (RecordEntity contact : contact.getRecords()) {
            System.out.println("\t" + contact.getType().getDescription() + ": " + contact.getValue());
        }

        if (contactRepository.findById(contact.getUuid()).isEmpty())
            isBack = true;
    }

    @Override
    public List<MenuItem> getMenu() {
        return List.of(
                new DeleteUserMenuItem(contact, contactRepository),
                new BackMenuItem()
        );
    }

    @Override
    public boolean isBack() {

        if (isBack)
            System.out.println("Contact has been deleted!");

        return isBack;
    }
}
