package com.phone.ui.menu.items;

public interface MenuItem {

    String getName();
    default boolean isFinish() { return false; }
    default boolean isBack() { return false; }

}
