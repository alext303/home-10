package com.phone.ui.menu.items;

public class ExitMenuItem implements MenuItem {
    @Override
    public String getName() {
        return "Exit";
    }

    @Override
    public boolean isFinish() {
        return true;
    }
}
