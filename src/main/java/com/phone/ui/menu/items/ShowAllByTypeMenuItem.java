package com.phone.ui.menu.items;

import com.phone.repository.ContactRepository;
import com.phone.repository.model.ContactEntity;
import com.phone.repository.model.TypeContact;
import com.phone.service.RegisterLoginInputService;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ShowAllByTypeMenuItem implements RunnableMenuItem, AddMenu {

    private final TypeContact type;

    private final ContactRepository contactRepository;

    private final RegisterLoginInputService loginInputService;

    private final List<MenuItem> menuContacts = new ArrayList<>();

    @Override
    public String getName() {
        return "Show all " + (type == null ? "Contacts" : type.name());
    }

    @Override
    public void run() {

        menuContacts.clear();

        List<ContactEntity> contacts = contactRepository.findAllByOwner(loginInputService.getOwner())
                .stream()
                .sorted(Comparator.comparing(ContactEntity::getSurname))
                .filter(c -> c.getRecords().stream().anyMatch(r -> {
                    if (type != null)
                        return r.getType() == type;
                    else
                        return true;
                }))
                .collect(Collectors.toList());

        if (contacts.size() == 0) {
            System.out.println("Contacts not found");
        } else
            for (ContactEntity userModel : contacts)
                this.menuContacts.add(new UserMenuItem(userModel, contactRepository));

        this.menuContacts.add(new BackMenuItem());
    }

    @Override
    public List<MenuItem> getMenu() {
        return menuContacts;
    }
}
