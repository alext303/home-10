package com.phone.ui.menu.items;

import com.phone.repository.ContactRepository;
import com.phone.repository.model.ContactEntity;
import com.phone.repository.model.RecordEntity;
import com.phone.service.RegisterLoginInputService;
import com.phone.service.SearchInputService;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class SearchByTypeSearchContainsMenuItem implements RunnableMenuItem {

    private final TypeSearch typeSearch;

    private final ContactRepository contactRepository;

    private final RegisterLoginInputService registerLoginInputService;
    private final SearchInputService searchInputService;

    @Override
    public String getName() {
        return "Search by " + typeSearch.name();
    }

    @Override
    public void run() {
        String search = searchInputService.getSearchString();

        List<ContactEntity> temp = contactRepository.findAllByOwner(registerLoginInputService.getOwner());
        List<ContactEntity> contacts = temp.stream()
                .filter(u -> {
                    if (typeSearch == TypeSearch.RECORD) {
                        return u.getRecords().stream()
                                .anyMatch(c -> c.getValue().toLowerCase().contains(search.toLowerCase()));
                    }
                    return u.getName().toLowerCase().contains(search.toLowerCase()) ||
                            u.getSurname().toLowerCase().contains(search.toLowerCase()) ||
                            u.getPatronymic().toLowerCase().contains(search.toLowerCase());

                }).collect(Collectors.toList());

        if (contacts.size() > 0) {
            System.out.println("Search result: ");
            for (ContactEntity user : contacts) {
                System.out.printf("Name: %s\n", user.getName());
                System.out.printf("Surname: %s\n", user.getSurname());
                System.out.printf("Patronymic: %s\n", user.getPatronymic());

                for (RecordEntity contact : user.getRecords()) {
                    System.out.printf("\t%s: %s", contact.getType().getDescription(), contact.getValue());
                }
                System.out.println("\n");
            }
        } else
            System.out.println("Users not found");
    }
}
