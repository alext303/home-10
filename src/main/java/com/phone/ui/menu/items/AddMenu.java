package com.phone.ui.menu.items;

import java.util.List;

public interface AddMenu {

    List<MenuItem> getMenu();

}
