package com.phone.ui.menu.items;

import com.phone.repository.OwnerRepository;
import com.phone.service.RegisterLoginInputService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class RegisterMenuItem implements RunnableMenuItem {

    private final RegisterLoginInputService registerLoginInputService;
    private final OwnerRepository ownerRepository;

    private boolean isBack = false;

    @Override
    public String getName() {
        return "Register";
    }

    @Override
    public void run() {
        while (true) {
            isBack = true;
            break;
        }
    }

    @Override
    public boolean isBack() {
        return isBack;
    }
}
