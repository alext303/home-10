package com.phone.ui.menu;


import com.phone.repository.ContactRepository;
import com.phone.repository.OwnerRepository;
import com.phone.repository.model.TypeContact;
import com.phone.service.*;
import com.phone.ui.menu.items.*;
import com.phone.utils.ViewScanner;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Scanner;

@RequiredArgsConstructor
public class MenuUI {

    private final Scanner scanner;
    private final List<List<MenuItem>> menu;

    private final ContactRepository contactRepository;
    private final OwnerRepository ownerRepository;

    private void show() {
        System.out.println("\n-------------------");
        for (int i = 0; i < getMenu().size(); i++) {
            System.out.printf("%d - %s\n", i + 1, getMenu().get(i).getName());
        }
        System.out.println("-------------------");
    }

    private boolean runItem(int index) {
        if (!validate(index)) {
            System.out.println("Incorrect input, try again");
            return true;
        }
        run(index);
        return !getMenu().get(index).isFinish();
    }

    private void changeMenu(int index) {
        if (validate(index)) {
            MenuItem menuItem = getMenu().get(index);
            if (menuItem.isBack())
                menu.remove(menu.size() - 1);
            else if (menuItem instanceof AddMenu)
                if (((AddMenu) menuItem).getMenu().size() > 0)
                    menu.add(((AddMenu) menuItem).getMenu());
        }
    }

    private void run(int index) {
        MenuItem menuItem = getMenu().get(index);
        if (!(menuItem instanceof Runnable)) return;
        ((Runnable) menuItem).run();
    }

    private List<MenuItem> getMenu() {
        return menu.get(menu.size() - 1);
    }

    private boolean validate(int index) {
        return index >= 0 && index < getMenu().size();
    }

    private int choose() {
        System.out.print("Enter your choose: ");
        int choose = -1;

        try {
            choose = scanner.nextInt();
        } catch (Exception ignore) {
        }

        scanner.nextLine();
        return choose;
    }

    public void init() {

        ViewScanner viewScanner = new ViewScanner(scanner);

        RegisterLoginInputService registerLoginInputService = new RegisterLoginInputServiceImpl(viewScanner);
        AddUserInputService addUserInputService = new AddUserInputServiceImpl(viewScanner);
        SearchInputService searchInputService = new SearchInputServiceImpl(viewScanner);

        //Main menu
        menu.add(List.of(
                        new ShowAllByTypeMenuItem(null, contactRepository, registerLoginInputService),
                        new ShowAllByTypeMenuItem(TypeContact.PHONE, contactRepository, registerLoginInputService),
                        new ShowAllByTypeMenuItem(TypeContact.EMAIL, contactRepository, registerLoginInputService),
                        new SearchUserMenuItem(contactRepository, searchInputService, registerLoginInputService),
                        new AddUserMenuItem(contactRepository, addUserInputService, registerLoginInputService),
                        new ExitMenuItem()
                )
        );

        //Login/Register Menu
        menu.add(List.of(
                new LoginMenuItem(registerLoginInputService, ownerRepository),
                new RegisterMenuItem(registerLoginInputService, ownerRepository),
                new ExitMenuItem())
        );

        while (true) {
            show();
            int index = choose() - 1;
            if (!runItem(index)) break;
            changeMenu(index);
        }
    }
}
