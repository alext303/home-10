package com.phone.service;

import com.phone.repository.model.TypeContact;
import com.phone.utils.ViewScanner;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class AddUserInputServiceImpl implements AddUserInputService {

    private final ViewScanner viewScanner;

    @Override
    public String getName() {
        return viewScanner.getString("Input user Name: ");
    }

    @Override
    public String getSurname() {
        return viewScanner.getString("Input user Surname: ");
    }

    @Override
    public String getPatronymic() {
        return viewScanner.getString("Input user Patronymic: ");
    }

    @Override
    public TypeContact getTypeContact() {

        for (int i = 0; i < TypeContact.values().length; i++) {
            System.out.printf("%d - %s\n", i + 1, TypeContact.values()[i].getDescription());
        }

        int input = viewScanner.getInt("Input type: ");

        viewScanner.nextLine();

        switch (input) {
            case 2:
                return TypeContact.EMAIL;
            case 3:
                return TypeContact.ADDRESS;
            default:
                return TypeContact.PHONE;
        }

    }

    @Override
    public String getValueContact(TypeContact type) {
        return viewScanner.getString("Input " + type.getDescription() + ": ");
    }

    @Override
    public boolean getMoreContact() {
        int input = viewScanner.getInt("Add more contact 1- yes, another - no: ");
        viewScanner.nextLine();
        return input == 1;
    }
}
