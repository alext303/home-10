package com.phone.service;

import com.phone.utils.ViewScanner;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class RegisterLoginInputServiceImpl implements RegisterLoginInputService {

    private final ViewScanner scanner;
    @Getter
    private String owner;

    @Override
    public String getLogin() {
        owner = scanner.getString("Input Login: ");
        return owner;
    }

    @Override
    public String getPassword() {
        return scanner.getString("Input Password: ");
    }

    @Override
    public String getOwner() {
        return owner;
    }
}
