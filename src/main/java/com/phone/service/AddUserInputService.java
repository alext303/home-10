package com.phone.service;

import com.phone.repository.model.TypeContact;

public interface AddUserInputService {

    String getName();
    String getSurname();
    String getPatronymic();
    TypeContact getTypeContact();
    String getValueContact(TypeContact type);
    boolean getMoreContact();
}
