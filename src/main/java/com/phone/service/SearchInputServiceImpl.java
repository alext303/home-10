package com.phone.service;

import com.phone.utils.ViewScanner;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SearchInputServiceImpl implements SearchInputService {

    private final ViewScanner scanner;

    @Override
    public String getSearchString() {
        return scanner.getString("Input search: ") ;
    }
}
