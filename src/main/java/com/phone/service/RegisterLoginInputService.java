package com.phone.service;

public interface RegisterLoginInputService {

    String getLogin();
    String getPassword();
    String getOwner();
}
