package com.phone.service;

public interface SearchInputService {

    String getSearchString();

}