package com.phone.utils;

import lombok.RequiredArgsConstructor;

import java.util.Scanner;

@RequiredArgsConstructor
public class ViewScanner {

    private final Scanner scanner;

    public String getString(String description) {
        System.out.print(description);
        return scanner.nextLine();
    }

    public Integer getInt(String description) {
        System.out.print(description);
        return scanner.nextInt();
    }

    public Double getDouble(String description) {
        System.out.print(description);
        return scanner.nextDouble();
    }

    public void nextLine() {
        scanner.nextLine();
    }
}
