package com.phone.repository;

import com.phone.repository.model.LoginRegisterModel;

public interface OwnerRepository {

    boolean login(LoginRegisterModel user);

    boolean register(LoginRegisterModel user);

}
