package com.phone.repository.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum TypeContact {

    PHONE("Mob. Phone"),
    EMAIL("E-mail"),
    ADDRESS("St. Address");

    @Getter
    private final String description;
}
