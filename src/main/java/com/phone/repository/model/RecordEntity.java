package com.phone.repository.model;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class RecordEntity {

    private String uuid;
    private TypeContact type;
    private String value;
    private String userUuid;

}
