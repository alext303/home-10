package com.phone.repository.model;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
public class ContactEntity {
    private String uuid;
    private String name;
    private String surname;
    private String patronymic;
    private String owner;
    private List<RecordEntity> records;

    public ContactEntity addRecord(RecordEntity record) {
        if (records == null)
            records = new ArrayList<>();
        this.records.add(record);
        return this;
    }
}
