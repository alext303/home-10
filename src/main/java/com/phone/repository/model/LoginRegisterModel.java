package com.phone.repository.model;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class LoginRegisterModel implements Serializable {
    private String login;
    private String password;
}
