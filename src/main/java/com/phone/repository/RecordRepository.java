package com.phone.repository;

import com.phone.repository.model.RecordEntity;
import com.phone.repository.model.ContactEntity;

import java.util.List;

public interface RecordRepository {

    List<RecordEntity> findAllByUserId(String uuid);

    void saveAll(List<RecordEntity> recordEntities);

    void deleteAllByUser(ContactEntity user);

    boolean delete(RecordEntity recordEntity);

}
