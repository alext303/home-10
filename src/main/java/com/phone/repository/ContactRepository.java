package com.phone.repository;

import com.phone.repository.model.ContactEntity;

import java.util.List;
import java.util.Optional;

public interface ContactRepository {

    List<ContactEntity> findAll();

    List<ContactEntity> findAllByOwner(String owner);

    ContactEntity save(ContactEntity user);

    void delete(ContactEntity user);

    Optional<ContactEntity> findById(String uuid);
}
