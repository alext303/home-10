package com.phone.repository.rowMappers;

import com.phone.repository.model.RecordEntity;
import com.phone.repository.model.TypeContact;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ContactsRowMapper implements RowMapper<RecordEntity> {
    @Override
    public RecordEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new RecordEntity()
                .setUuid(rs.getString("uuid"))
                .setType(TypeContact.valueOf(rs.getString("type")))
                .setUserUuid(rs.getString("user_uuid"))
                .setValue(rs.getString("contact"));
    }
}
