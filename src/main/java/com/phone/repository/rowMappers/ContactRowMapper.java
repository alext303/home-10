package com.phone.repository.rowMappers;

import com.phone.repository.RecordRepository;
import com.phone.repository.model.ContactEntity;
import lombok.RequiredArgsConstructor;

import java.sql.ResultSet;
import java.sql.SQLException;

@RequiredArgsConstructor
public class ContactRowMapper implements RowMapper<ContactEntity> {

    private final RecordRepository recordRepository;

    @Override
    public ContactEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new ContactEntity()
                .setOwner(rs.getString("owner"))
                .setUuid(rs.getString("uuid"))
                .setPatronymic(rs.getString("patronymic"))
                .setName(rs.getString("name"))
                .setRecords(recordRepository.findAllByUserId(rs.getString("uuid")))
                .setSurname(rs.getString("surname"));
    }
}
